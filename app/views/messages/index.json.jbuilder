json.array!(@messages) do |message|
  json.extract! message, :id, :title, :username, :content
  json.url message_url(message, format: :json)
end
