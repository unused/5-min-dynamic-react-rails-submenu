
var Submenu = React.createClass({

  loadItems: function() {
    $.ajax({
      url: this.props.ressource,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data, count: data.length});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.ressource, status, err.toString());
      }.bind(this)
    });
  },

  getInitialState: function(){
    return { data: [], count: 0 };
  },

  componentDidMount: function() {
    this.loadItems(); setInterval(this.loadItems, 500);
  },

  render: function() {
    return (
      <div class="submenu">
        <h3><span>{this.state.count}</span> {this.props.label}</h3>
        <ol>
          { this.state.data.map(function(message, index) {
            return (
              <li key={index}>
                <h3>{message.title}</h3>
                <p>Message from {message.username}: "{message.content}"</p>
              </li>
            )
          })}
        </ol>
      </div>
    );
  }
});
