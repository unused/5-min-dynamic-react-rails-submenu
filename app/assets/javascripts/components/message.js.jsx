var Message = React.createClass({
  propTypes: {
    title: React.PropTypes.node,
    username: React.PropTypes.node,
    content: React.PropTypes.node
  },

  render: function() {
    return (
      <div>
        <div>Title: {this.props.title}</div>
        <div>Username: {this.props.username}</div>
        <div>Content: {this.props.content}</div>
      </div>
    );
  }
});

